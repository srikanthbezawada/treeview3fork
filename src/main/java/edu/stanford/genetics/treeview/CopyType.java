package edu.stanford.genetics.treeview;

public enum CopyType {
	ALL, SELECTION, VISIBLE_MATRIX
}
