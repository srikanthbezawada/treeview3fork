/* BEGIN_HEADER                                                   TreeView 3
 *
 * Please refer to our LICENSE file if you wish to make changes to this software
 *
 * END_HEADER 
 */
package edu.stanford.genetics.treeview;

public class NoValueException extends Exception {// when no value can be
	// returned...

	private static final long serialVersionUID = 1L;

	public NoValueException(final String s) {

		super(s);
	}
}
