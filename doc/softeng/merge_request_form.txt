COMMIT/MERGE/PULL FORM
Version 1.2
Prepared by: Robert Leach

PURPOSE: This template is for use when submitting a merge/pull request.

REFERENCES:
[1] change_request_form.txt version 1.2
[2] document_review_form.txt version 1.3
[3] SCMP_TreeView3.doc, section 3.2.1 CHANGE CONTROL OVERVIEW PROCEDURE
[4] SCMP_TreeView3.doc, section 3.2.6.3.1.1 MERGE REQUEST NECESSITY DETERMINATION
[5] SCMP_TreeView3.doc, section 3.2.6.3.1.2 MERGE REQUEST PROCEDURE

BITBUCKET PULL REQUEST FORM[3]
==============================

DATE OF SUBMISSION: 

BITBUCKET ISSUE NUMBER(S): #

LINK TO JAR FILE: 

LEVEL OF EFFORT: trivial/minor/medium/major/overhaul (choose one)

DESCRIPTION OF CHANGES:



MANUAL COMMIT/MERGE REQUEST FORM[4]
===================================

           AUTHOR NAME: 
    DATE OF SUBMISSION: 
BITBUCKET ISSUE NUMBER: #
   CHANGED BRANCH NAME: 
      LINK TO JAR FILE: 
       BRANCH LOCATION: local/remote (choose one)
       LEVEL OF EFFORT: trivial/minor/medium/major/overhaul (choose one)

LIST OF FILES CHANGED AND THEIR PSL/PDL LOCATIONS:


DESCRIPTION OF CHANGES:


NOTE: INCLUDE A COPY OF NEW FILES & THE OUTPUT OF `git diff <filename>` ON ALL
CHANGED FILES WITH THE SUBMISSION OF THIS FORM

